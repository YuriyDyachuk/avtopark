<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Auto;
use App\Models\AutoParking;
use Faker\Generator as Faker;

$factory->define(Auto::class, function (Faker $faker) {
    return [
        'number_cars' => $faker->randomNumber(4),
        'driver_name' => $faker->name,
    ];
});
