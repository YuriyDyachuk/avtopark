<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Auto;
use App\Models\AutoParking;
use App\Models\Parking;
use Faker\Generator as Faker;

$factory->define(AutoParking::class, function (Faker $faker) {
    return [
        'parking_id' =>  factory(Parking::class),
        'auto_id' => factory(Auto::class),
    ];
});
