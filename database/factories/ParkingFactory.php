<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AutoParking;
use App\Models\Parking;
use Faker\Generator as Faker;

$factory->define(Parking::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'address' => $faker->address,
        'schedule' => $faker->date(),
    ];
});
