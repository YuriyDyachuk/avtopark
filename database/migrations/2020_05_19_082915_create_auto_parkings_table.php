<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_parkings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('auto_id')->nullable();
            $table->unsignedBigInteger('parking_id')->nullable();
            $table->foreign('auto_id')
                ->references('id')->on('autos')
                ->onDelete('cascade');
            $table->foreign('parking_id')
                ->references('id')->on('parkings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_parkings');
    }
}
