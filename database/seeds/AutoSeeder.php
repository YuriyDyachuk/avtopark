<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::find(1);
        $data = [
            [
                'id' => 1,
                'user_id' => $user->id,
                'number_cars' => 'FF5050FF',
                'driver_name' => 'Jon'
            ],
            [
                'id' => 2,
                'user_id' => $user->id,
                'number_cars' => 'FF5115FF',
                'driver_name' => 'Max'
            ]
        ];
        DB::table('autos')->insert($data);
    }
}
