<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParkingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Parking 1',
                'address' => ' Wool street 25',
                'schedule' => '07:00-22:00',
            ],
            [
                'id' => 2,
                'name' => 'Parking 2',
                'address' => ' Wool street 59',
                'schedule' => '07:00-22:00',
            ]
        ];
        DB::table('parkings')->insert($data);
    }
}
