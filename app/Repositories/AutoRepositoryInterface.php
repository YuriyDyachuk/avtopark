<?php

namespace App\Repositories;

use App\Models\Auto;

interface AutoRepositoryInterface
{
    /**
     * @param null $perPage
     * @return mixed
     */
    public function allAuto($perPage = null);

    /**
     * @param $fields
     * @return Auto
     */
    public function addAuto($fields);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
