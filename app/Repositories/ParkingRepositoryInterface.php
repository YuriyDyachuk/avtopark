<?php

namespace App\Repositories;

interface ParkingRepositoryInterface
{
    /**
     * @param null $perPage
     * @return mixed
     */
    public function allParking($perPage = null);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     */
    public function addNewAuto($id);
}
