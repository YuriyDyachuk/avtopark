<?php

namespace App\Repositories;

use App\Models\Auto;

class NewAutoUserRepository
{
    /**
     * @return mixed
     */
    public function allAuto()
    {
        $all_auto = Auto::orderBy('id')
            ->with('parking')
            ->where('user_id', auth()->user()->id)
            ->get()
            ->toArray();

        return $all_auto;
    }

    /**
     * @param $fields
     * @return Auto
     */
    public function addNewAuto($fields)
    {
        return Auto::add($fields);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findByAutoId($id)
    {
        return Auto::orderBy('id')
            ->where('id',$id)
            ->with('parking')
            ->firstOrFail();
    }

}
