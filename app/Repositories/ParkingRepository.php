<?php

namespace App\Repositories;

use App\Models\Parking;

class ParkingRepository implements ParkingRepositoryInterface
{
    /**
     * @param null $perPage
     * @return mixed
     */
    public function allParking($perPage = null)
    {
        $s = request('s');
        $fields = ['id', 'name', 'address', 'schedule'];

        return Parking::select($fields)
            ->search($s)
//            ->orderByDesc('id')
            ->paginate($perPage);
//        $customers = Parking::orderBy('name')
//            ->with('auto')
//            ->map(function ($customer) {
//                return [
//                 'id'=>$customer->id,
//                 'name'=>$customer->name,
//                 'address'=>$customer->address,
//                 'schedule'=>$customer->schedule,
//               ];
//            })
//            ->paginate($perPage);
//        return $customers;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Parking::orderBy('name')
            ->where('id', $id)
            ->with('auto')
            ->firstOrFail();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return Parking::find($id)->delete();
    }

    /**
     * @param $id
     */
    public function addNewAuto($id)
    {
        $new_auto = \request('car');
        if ($new_auto === null){
            return back()->withErrors(['msg' => 'Упс!']);
        }
        if (!empty($new_auto)){
            $parking = $this->findById($id);
            $res = array_chunk($new_auto, 2);
            $option[] = $res;
            foreach ($option as $key => $val) {
                foreach ($val as $item) {
                    if ($item[0] == '' && $item[1] == ''
                        || $item[0] != '' && $item[1] == ''
                        || $item[0] == '' && $item[1] != ''){
                        return back()->withErrors(['msg' => 'Упс!']);
                    }
                    $parking->auto()->create(array(
                        'number_cars' => $item[0],
                        'driver_name' => $item[1]
                    ));
                }
            }
            return back()->with(['success' => 'Автомобиль успешно добавлен']);
        }
    }

}
