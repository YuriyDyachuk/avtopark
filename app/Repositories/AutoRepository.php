<?php

namespace App\Repositories;

use App\Models\Auto;

class AutoRepository implements AutoRepositoryInterface
{
    /**
     * @param null $perPage
     * @return mixed
     */
    public function allAuto($perPage = null)
    {
        $s = request('s');
        $fields = ['id','number_cars','driver_name'];

        return Auto::select($fields)
            ->search($s)
            ->orderBYdesc('id')
            ->paginate($perPage);
    }

    /**
     * @param $fields
     * @return Auto
     */
    public function addAuto($fields)
    {
        return Auto::add($fields);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return Auto::orderBy('driver_name')
            ->where('id',$id)
            ->with('parking')
            ->firstOrFail();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return Auto::findOrFail($id)->delete();
    }
}
