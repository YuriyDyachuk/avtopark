<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoParking extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['auto_id', 'parking_id'];
}
