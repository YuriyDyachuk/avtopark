<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['name','address', 'schedule'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function auto()
    {
        return $this->belongsToMany(
          Auto::class,
          'auto_parkings',
            'parking_id',
            'auto_id'
        );
    }

    /**
     * @param $fields
     * @return static
     * Добавление парковки
     */
    public static function add($fields)
    {
        $parking = new static();
        $parking->fill($fields);
        $parking->save();

        return $parking;
    }

    /**
     * @param $fields
     * Редактирование парковки
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     * Удаление парковки
     */
    public function remove()
    {
        $this->delete();
    }

    /**
     * @param $id
     * Сохранение авто к парковкам
     * принимаем массив из id [1,3,5,6 ....]
     */
    public function setAuto($id)
    {
        if ($id == null){return;}
        $this->auto()->sync($id);
    }

    /**
     * @param $query
     * @param $s
     * @return mixed
     */
    public function scopeSearch($query , $s)
    {
        $term = "%$s%";
        return $query->where('name','like', $term)
            ->orWhere('address', 'like', $term);
    }
}
