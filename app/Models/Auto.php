<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Auto extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['user_id', 'number_cars', 'driver_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parking()
    {
        return $this->belongsToMany(
            Parking::class,
            'auto_parkings',
            'auto_id',
            'parking_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this->hasOne(User::class);
    }


    /**
     * @param $fields
     * @return static
     * Добавление записи
     */
    public static function add($fields)
    {
        $auto = new static();
        $auto->user_id = auth()->user()->id;
        $auto->fill($fields);
        $auto->save();

        return $auto;
    }

    /**
     * @param $fields
     * Редактирование записи
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     * Удаление записи
     */
    public function remove()
    {
        $this->delete();
    }

    /**
     * @param $id
     * Сохранение парковок к авто
     * принимаем массив из id [1,3,5,6 ....]
     */
    public function setParking($id)
    {
        if ($id == null){return;}
        $this->parking()->sync($id);
    }

    /**
     * @return array
     * repository NewAutoRepository
     *      ->map(function) {
     *          return [
     *              bla-bla .......
     *          ];
     *      }
     *
     */
    public function format()
    {
        return [
            'id' => $this->id,
            'number_cars' => $this->number_cars,
            'driver_name' => $this->driver_name,
        ];
    }


    /**
     * @param $query
     * @param $s
     * @return mixed
     */
    public function scopeSearch($query , $s)
    {
        $term = "%$s%";
        return $query->where('number_cars','like', $term)
               ->orWhere('driver_name', 'like', $term);
    }
}
