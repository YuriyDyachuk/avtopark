<?php

namespace App\Providers;

use App\Repositories\AutoRepository;
use App\Repositories\AutoRepositoryInterface;
use App\Repositories\ParkingRepository;
use App\Repositories\ParkingRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ParkingRepositoryInterface::class,ParkingRepository::class);
        $this->app->bind(AutoRepositoryInterface::class,AutoRepository::class);
    }
}
