<?php

namespace App\Observers;

use App\Models\Parking;
use App\Repositories\ParkingRepository;
use function GuzzleHttp\Promise\all;
use function Sodium\add;

class ParkingObserver
{
    /**
     * Handle the parking "created" event.
     *
     * @param  \App\Models\Parking  $parking
     * @return \Illuminate\Http\RedirectResponse
     */
    public function created(Parking $parking)
    {
        $new_auto = request('car');
        if ($new_auto === null){
            return back()->withErrors(['msg' => 'Упс!']);
        }
        if (!empty($new_auto)) {
            $res = array_chunk($new_auto, 2);
            $option = $res;
            foreach ($option as $key => $item) {
                if ($item[0] == '' && $item[1] == ''
                    || $item[0] != '' && $item[1] == ''
                    || $item[0] == '' && $item[1] != ''){
                    return back()->withErrors(['msg' => 'Упс!']);
                }
                $parking->auto()->create([
                    'number_cars' => $item[0],
                    'driver_name' => $item[1]
                ]);
            }
        }
    }

    /**
     * Handle the parking "updated" event.
     *
     * @param  \App\Models\Parking  $parking
     * @return void
     */
    public function updated(Parking $parking)
    {
        //
    }

    /**
     * Handle the parking "deleted" event.
     *
     * @param  \App\Models\Parking  $parking
     * @return void
     */
    public function deleted(Parking $parking)
    {
        //
    }

    /**
     * Handle the parking "restored" event.
     *
     * @param  \App\Models\Parking  $parking
     * @return void
     */
    public function restored(Parking $parking)
    {
        //
    }

    /**
     * Handle the parking "force deleted" event.
     *
     * @param  \App\Models\Parking  $parking
     * @return void
     */
    public function forceDeleted(Parking $parking)
    {
        //
    }
}
