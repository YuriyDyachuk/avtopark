<?php

namespace App\Http\Controllers;

use App\Http\Requests\AutoCreateValidator;
use App\Repositories\NewAutoUserRepository;

class NewAutoUserController extends Controller
{
    /**
     * @var NewAutoUserRepository
     */
    private $newAutoUserRepository;

    /**
     * NewAutoUserController constructor.
     * @param NewAutoUserRepository $newAutoUserRepository
     */
    public function __construct(NewAutoUserRepository $newAutoUserRepository)
    {
        $this->newAutoUserRepository = $newAutoUserRepository;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param NewAutoUserRepository $newAutoUserRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(NewAutoUserRepository $newAutoUserRepository)
    {
        $res = $this->newAutoUserRepository->allAuto();
        return view('page.index',['res' => $res]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('page.create');
    }


    /**
     * @param AutoCreateValidator $request
     * @param NewAutoUserRepository $newAutoUserRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AutoCreateValidator $request, NewAutoUserRepository $newAutoUserRepository)
    {
        $form = $request->only('number_cars','driver_name');
        $res = $this->newAutoUserRepository->addNewAuto($form);
        if (!$res) {
            return back()->withErrors(['msg' => 'Ошибка добавления!'])->exceptInput();
        }
        return redirect()->route('new-auto.index')->with(['success' => 'Автомобиль успешно добавлен']);
    }


    /**
     * @param $id
     * @param NewAutoUserRepository $newAutoUserRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, NewAutoUserRepository $newAutoUserRepository)
    {
        $res = $this->newAutoUserRepository->findByAutoId($id);
        return view('page.edit',['auto' => $res]);
    }


    /**
     * @param AutoCreateValidator $request
     * @param $id
     * @param NewAutoUserRepository $newAutoUserRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AutoCreateValidator $request, $id, NewAutoUserRepository $newAutoUserRepository)
    {
        $form = $request->only('number_cars','driver_name');
        $res = $this->newAutoUserRepository->findByAutoId($id);
        $res->edit($form);
        if (!$res) {
            return back()->withErrors(['msg' => 'Ошибка обновления!'])->withInput();
        }
        return back()->with(['success' => 'Запись успешно обновлена']);
    }

}
