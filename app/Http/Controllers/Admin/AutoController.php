<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AutoCreateValidator;
use App\Http\Requests\NewAutoCreateValidator;
use App\Http\Requests\UpdateAutoValidator;
use App\Models\Auto;
use App\Repositories\AutoRepository;
use App\Repositories\AutoRepositoryInterface;
use Illuminate\Http\Request;

class AutoController extends Controller
{
    /**
     * @var AutoRepository
     */
    private $autoRepository;


    /**
     * AutoController constructor.
     * @param AutoRepositoryInterface $autoRepository
     */
    public function __construct(AutoRepositoryInterface $autoRepository)
    {
        $this->autoRepository = $autoRepository;
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $autos = $this->autoRepository->allAuto(10);
        return view('admin.auto.index', ['autos' => $autos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.auto.create');
    }


    /**
     * @param NewAutoCreateValidator $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewAutoCreateValidator $request)
    {
        $auto = $request->only('number_cars','driver_name');
        $result = $this->autoRepository->addAuto($auto);
        if (!isset($result)) {
            return back()->withErrors(['msg' => 'Ошибка создания!'])->withInput();
        }else {
            return redirect()->route('auto.index')->with(['success' => 'Запись успешно создана']);
        }
    }


    /**
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $result = $this->autoRepository->findById($id);
        return view('admin.auto.edit',['result'=>$result]);
    }


    /**
     * @param UpdateAutoValidator $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateAutoValidator $request, int $id)
    {
        $forms = $request->only('number_cars', 'driver_name');
        $result = $this->autoRepository->findById($id);
        $result->edit($forms);
        if (!$result) {
            return back()->withErrors(['msg' => 'Ошибка обновления!'])->withInput();
        }else {
            return back()->with(['success' => 'Автомобиль успешно обновлен']);
        }
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $result = $this->autoRepository->delete($id);
        if (!isset($result)) {
            return back()->withErrors(['msg' => 'Ошибка удаления!'])->withInput();
        }else {
            return back()->with(['success' => 'Автомобиль успешно удален!']);
        }
    }
}
