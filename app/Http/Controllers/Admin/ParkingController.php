<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AutoCreateValidator;
use App\Http\Requests\ParkingCreateValidator;
use App\Models\Parking;
use App\Repositories\ParkingRepository;
use App\Repositories\ParkingRepositoryInterface;

class ParkingController extends Controller
{

    /**
     * @var ParkingRepository
     */
    private $parkingRepository;

    /**
     * ParkingController constructor.
     * @param ParkingRepositoryInterface $parkingRepository
     */
    public function __construct(ParkingRepositoryInterface $parkingRepository)
    {
        $this->parkingRepository = $parkingRepository;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customers = $this->parkingRepository->allParking(10);
        return view('admin.parking.index', ['parking' => $customers,]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function create()
    {
        return view('admin.parking.create');
    }

    /**
     * @param ParkingCreateValidator $request
     * @param Parking $parking
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ParkingCreateValidator $request, Parking $parking)
    {
        $forms = $request->only('name', 'address', 'schedule');
        $result = $parking::add($forms);
        if (!$result) {
            return back()->withErrors(['msg' => 'Ошибка создания!'])->withInput();
        } else {
            return redirect()->route('parking.index')->with(['success' => 'Запись успешно создана']);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $avtopark = $this->parkingRepository->findById($id);
        return view('admin.parking.edit', ['avtopark' => $avtopark]);
    }

    /**
     * @param ParkingCreateValidator $request
     * @param AutoCreateValidator $autoCreateValidator
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ParkingCreateValidator $request, AutoCreateValidator $autoCreateValidator, $id)
    {
        $forms = $request->only('name', 'address', 'schedule');
        $result = $this->parkingRepository->findById($id);
        $auto = $this->parkingRepository->addNewAuto($id);
        if ($auto) {return back()->with(['success'=>'']);}
        $result->edit($forms);
        if (!$result) {
            return back()->withErrors(['msg' => 'Ошибка обновления!'])->withInput();
        } else {
            return back()->with(['success' => 'Запись успешно обновлена']);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $result = $this->parkingRepository->delete($id);
        if (!$result) {
            return back()->withErrors(['msg' => 'Ошибка удаления!'])->withInput();
        } else {
            return redirect()->route('parking.index')->with(['success' => 'Запись успешно удалена']);
        }
    }
}
