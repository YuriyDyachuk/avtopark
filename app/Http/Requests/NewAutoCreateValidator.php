<?php

namespace App\Http\Requests;

use App\Models\Auto;
use Illuminate\Foundation\Http\FormRequest;

class NewAutoCreateValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number_cars' => 'required',
            'driver_name' => 'required'
        ];
    }
}
