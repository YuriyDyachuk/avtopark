<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::group(['middleware' => 'new.auto'],
    function () {
        Route::resource('/new-auto', 'NewAutoUserController');
    });

Route::group([
    'prefix' => 'admin',
    'middleware' => 'admin',
    'namespace' => 'Admin'],
    function () {
        Route::get('/', 'AdminDashboardController@index')->name('admin.index');
        Route::resource('/parking', 'ParkingController');
        Route::resource('/auto', 'AutoController');
        Route::resource('/user', 'UserController');
    });
