$(document).ready( function(){
    $('.alert').hide();

    $("form").on('submit', function(e){
        e.preventDefault();
        let $this = $(this);

        $.ajax({
            url: $this.prop('action'),
            method: 'POST',
            data: $this.serialize(),
            success: function (data) {
                $(document).ready(function () {
                    if (data.data)
                        $('.alert').text(data.data).show();
                    setTimeout(function() {
                        $('.alert').hide();
                        location.reload();
                    }, 2000);
                });
            },
            error: function (data) {
                $('.alert-danger').text(data.msg).show();
            }
        });
    });
});
