@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Перейти к регистрации автомобиля</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="form-group">
                            <a href="{{route('new-auto.index')}}" class="btn btn-success">Перейти к авто</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
