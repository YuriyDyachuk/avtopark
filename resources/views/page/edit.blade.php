@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="container-fluid">
        <!-- Main content -->
        <div class="offset-1 col-md-10">
            <br>
            <form action="{{route('new-auto.update', $auto->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="box-header">
                    <h3>Редактирование авто</h3>
                    @include('admin.errors')
                </div>
                <hr>
                <br>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Номер автомобиля</h5>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text"  class="form-control" value="{{$auto->number_cars}}"
                                       name="number_cars">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Имя водителя</h5>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{$auto->driver_name}}"
                                           name="driver_name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('new-auto.index') }}" class="btn btn-light">Назад</a>
                    <input type="hidden" name="id" value="{{$auto->id}}">
                    <button class="btn btn-success pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </form>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.content-wrapper -->
@endsection
