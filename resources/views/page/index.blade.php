@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="form-group">
            <a href="{{route('new-auto.create')}}" class="btn btn-success">Добавить авто</a>
        </div>
        <div class="box-header">
            @include('admin.errors')
        </div>
        <div class="row">
            <div class="offset-3 col-md-6">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Номер авто</th>
                        <th scope="col">Имя водителя</th>
                        <th scope="col">Автопарки авто</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($res as $val)
                        <tr>
                            <th scope="row">{{$val['id']}}</th>
                            <td>{{$val['number_cars']}}</td>
                            <td>{{$val['driver_name']}}</td>
                            <td>
                                @foreach ($val['parking'] as $item)
                                    {{$item['name'] . ' / '}}
                                @endforeach
                            </td>
                            <td>
                            <div class="form-group">
                                <a href="{{route('new-auto.edit',$val['id'])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
