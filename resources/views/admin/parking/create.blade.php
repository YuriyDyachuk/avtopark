@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <form action="{{route('parking.store')}}" method="POST">
                    @csrf
                    <div class="box-header">
                        <h3>Автопарк</h3>
{{--                        @include('admin.errors')--}}
                        {{--                        <div class="col-md-12 alert alert-success" role="alert"></div>--}}
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Название</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{$errors->has('name') ? 'has-error' : ''}}">
                                    <input type="text" id="name" class="form-control" value="{{old('name')}}"
                                           name="name">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                          <strong style="color: red">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Адрес</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group{{$errors->has('address') ? 'has-error' : ''}}">
                                        <input type="text" id="address" class="form-control" value="{{old('address')}}"
                                               name="address">
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                          <strong style="color: red">{{ $errors->first('address') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>График работы</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" id="schedule" class="form-control" value="{{old('schedule')}}"
                                           name="schedule">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Машины</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-push-1">
                                <table class="table table-dark information_json">
                                    <tr>
                                        <th><h5>Номер автомобиля</h5></th>
                                        <th><h5>Имя водителя</h5></th>
                                    </tr>
                                    <tr class="information_json_plus">
                                        <td></td>
                                        <td></td>
                                        <td><span class="btn btn-success plus pull-right">+</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ route('parking.index') }}" class="btn btn-default">Назад</a>
                        <button class="btn btn-success pull-right">Добавить</button>
                    </div>
                    <!-- /.box-footer-->
                </form>
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
