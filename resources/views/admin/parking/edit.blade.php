@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3>Редактирование автопарки {{$avtopark->name}}</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body container-fluid">
                    <form action="{{ route('parking.update',$avtopark->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Название</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{$avtopark->name}}" name="name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Адрес</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{$avtopark->address}}"
                                           name="address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>График работы</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{$avtopark->schedule}}"
                                           name="schedule">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Автомобили автопарка</h3>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-md-push-2">
                                    <table class="table information_json">
                                        <tr>
                                            <th><h5>Номер автомобиля</h5></th>
                                            <th><h5>Имя водителя</h5></th>
                                        </tr>
                                        <tr class="information_json_plus">
                                            <td></td>
                                            <td></td>
                                            <td><span class="btn btn-success plus pull-right">+</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="hidden" name="id" value="{{$avtopark->id}}">
                            <a href="{{ route('parking.index') }}" class="btn btn-default">Назад</a>
                            <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                        </div>
                    </form>
                    <!-- /.box-footer-->
                    <div class="col-xs-12 col-md-10 col-xl-10 col-lg-8 col-md-push-2">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Номер автомобиля</th>
                                <th>Имя водителя</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($avtopark->auto))
                                @foreach($avtopark->auto as $item)
                                    <tr>
                                        <form action="{{ route('auto.update',$item->id) }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                        <td><input type="text" name="number_cars" class="form-control"
                                                   value="{{$item->number_cars}}"></td>
                                        <td><input type="text" name="driver_name" class="form-control"
                                                   value="{{$item->driver_name}}"></td>
                                        <td>
                                            <input type="hidden" name="id" value="{{$item->id}}">
                                            <button type="submit" class="btn btn-success pull-right">Обновить</button>
                                        </td>
                                        </form>
                                        <td>
                                            <form action="{{ route('auto.update',$item->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            <button type="submit" onclick="return confirm('Вы точно хотите удалить данный автомобиль ?')" class="btn btn-danger pull-right">X</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
