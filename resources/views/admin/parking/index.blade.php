@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3>Список автопарков</h3>
                    @include('admin.errors')
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <a href="{{route('parking.create')}}" class="btn btn-success">Добавить</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-push-4">
                                <form action="{{ route('parking.index') }}" method="GET" class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="s" placeholder="Поиск">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Адрес</th>
                            <th>График работы</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($parking as $item)
                            <tr>
                                <td>{{$item['id']}}</td>
                                <td><a href="{{route('parking.edit', $item['id'])}}">{{$item['name']}}</a></td>
                                <td>{{$item['address']}}</td>
                                <td>{{$item['address']}}</td>
                                <td><a href="{{route('parking.edit', $item['id'])}}"><i class="fa fa-edit"
                                                                                         title="Редакировать"></i></a>
                                    <form action="{{ route('parking.destroy',$item['id']) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Вы точно желаете удалить данную автопарковку?')"
                                                type="submit" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash" title="Удалить" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            {{ $parking->links() }}

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
