@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                {!! Form::open(['route' => 'auto.store']) !!}
                <div class="box-header">
                    <h3>Автомобили</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Номер авто</h5>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{$errors->has('number_cars') ? 'has-error' : ''}}">
                                <input type="text" id="number_cars" class="form-control" value="{{old('number_cars')}}"
                                       name="number_cars">
                                @if ($errors->has('number_cars'))
                                    <span class="help-block">
                                          <strong style="color: red">{{ $errors->first('number_cars') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Имя водителя</h5>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{$errors->has('driver_name') ? 'has-error' : ''}}">
                                <input type="text" id="driver_name" class="form-control" value="{{old('driver_name')}}"
                                       name="driver_name">
                                @if ($errors->has('driver_name'))
                                    <span class="help-block">
                                          <strong style="color: red">{{ $errors->first('driver_name') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('auto.index') }}" class="btn btn-default">Назад</a>
                    <button class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
                {!! Form::close() !!}
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
