@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3>Список автомобилей</h3>
                    @include('admin.errors')
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <a href="{{route('auto.create')}}" class="btn btn-success">Добавить</a>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-push-4">
                                <form action="{{ route('auto.index') }}" method="GET" class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="s" placeholder="Поиск">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Номер автомобиля</th>
                            <th>Имя водителя</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($autos as $auto)
                            <tr>
                                <td>{{$auto->id}}</td>
                                <td><a href="{{route('auto.edit', $auto->id)}}">{{$auto->number_cars}}</a></td>
                                <td>{{$auto->driver_name}}</td>
                                <td><a href="{{route('auto.edit', $auto->id)}}"><i class="fa fa-edit"
                                                                                   title="Редакировать"></i></a>
                                    {{Form::open(['route'=>['auto.destroy', $auto->id], 'method'=>'delete'])}}
                                    <button onclick="return confirm('Вы точно желаете удалить данный автомобиль?')"
                                            type="submit" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" title="Удалить" aria-hidden="true"></i>
                                    </button>
                                    {{Form::close()}}

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            {{ $autos->links() }}

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
