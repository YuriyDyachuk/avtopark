@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <form action="{{ route('auto.update',$result->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="box-header">
                        <h3>Редактирование автомобиля {{$result->number_cars}}</h3>
                        @include('admin.errors')
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Номер авто</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{$result->number_cars}}"
                                           name="number_cars">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h5>Имя водителя</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{$result->driver_name}}"
                                           name="driver_name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Автопарки автомобиля</h3>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-md-push-2">
                                    <h5 style="text-align: center">Название автопарка</h5>
                                </div>
                                @if(!empty($result->parking))
                                    @foreach($result->parking as $item)
                                        <div class="col-md-8 col-md-push-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="{{$item->name}}"
                                                       name="name" id="name" readonly>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row" id="sample">
                            <app-component/>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ route('auto.index') }}" class="btn btn-default">Назад</a>
                        <input type="hidden" name="id" value="{{$result->id}}">
                        <button class="btn btn-warning pull-right">Изменить</button>
                    </div>
                    <!-- /.box-footer-->
                </form>
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
