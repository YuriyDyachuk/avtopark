<?php

namespace Tests\Feature\Models;

use App\Models\Auto;
use App\Models\Parking;
use App\Models\User;
use App\Repositories\ParkingRepository;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /** @test */
    public function adminIndexEx()
    {
        $response = $this->get('/admin/parking');
        $response->assertStatus(302);
    }

    /** @test */
    public function admin_parking()
    {
        $data = [];
        $test = (new ParkingRepository())->findById(1);
        $this->assertEquals("$test->name", 'Parking 1');
        $this->assertEquals("$test->auto", '[]');
    }

    /** @test */
    public function pagination_for_admin_parking_works()
    {
        //Page 1
        for ($i = 1; $i <= 10; $i++) {
            $res_1 = factory(Parking::class)->create([
                'name' => 'Parking ' . $i,
            ]);
        }

        //Page 2
        for ($i = 11; $i <= 20; $i++) {
            $res_2 = factory(Parking::class)->create([
                'name' => 'Parking ' . $i,
            ]);
        }

        // Page 1
        $response = $this->get('/admin/parking');
        $this->assertEquals("$res_1->name", 'Parking 10');
        $response->assertStatus(302);

        // Page 2
        $response = $this->get('/admin/parking?page=2');
        $this->assertEquals("$res_2->name", 'Parking 20');
        $response->assertStatus(302);
    }

    /** @test */
    public function auto_parking_id_add()
    {
        $user = User::find(1);
        //
        $park = factory(Parking::class)->create(['name' => 'ParkingAuto']);
        $auto = factory(Auto::class)->create(['user_id' => $user->id, 'driver_name' => $user->name]);

        // sync(id)
        $park->setAuto([$auto->id]);

        // is out
        $response = $this->get('/admin/parking');
        $response->assertStatus(302);
    }

}
