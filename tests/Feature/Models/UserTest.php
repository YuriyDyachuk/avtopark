<?php

namespace Tests\Feature\Models;

use App\Http\Middleware\Authenticate;
use App\Http\Middleware\NewAutoRegister;
use App\Models\Auto;
use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function login_form()
    {
        $this
            ->withoutMiddleware(NewAutoRegister::class)
            ->withoutMiddleware(Authenticate::class);

        $response = $this->get('/');

        $response
            ->assertSeeText('Перейти к авто')
            ->assertStatus(200);
    }

    /** @test */
    public function add_new_auto_user()
    {
        $this
            ->withoutMiddleware(NewAutoRegister::class)
            ->withoutMiddleware(Authenticate::class);

        $response = $this->get('/new-auto/create');
        $user = User::first();
        $res = factory(Auto::class)->create([
            'user_id' => $user->id,
            'driver_name' => $user->name
        ]);

        $this->assertEquals($res->user_id,$user->id);
        $response->assertStatus(200);
    }

    /**
     * @depends login_form
     * @test
     */
    public function landing_page_auto()
    {
        $this
            ->withoutMiddleware(NewAutoRegister::class)
            ->withoutMiddleware(Authenticate::class);

        $response = $this->get('/new-auto/1/edit');
        $auto = Auto::first();

        $response->assertSee('Редактирование авто');
        $this->assertEquals("$auto->number_cars",'FF5050FF');
        $response->assertStatus(200);
    }
}
